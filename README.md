### Libraries

## Scheduling

  - [Advanced Python Scheduler](https://apscheduler.readthedocs.io/en/latest/)

## Timezone

  - https://pendulum.eustace.io/docs/

## URLs

  - [furl](https://github.com/gruns/furl) URL parsing and manipulation

## Visualization

  - [Datashader](http://datashader.org/) 